# KDE Plasmoid for Jenkins

This plasmoid uses QtQuick 1.1 and KDE 4.

It is developed and tested with (Debian packages):

* Qt 4.8.6
* KDE 4.11.13

## Install

Clone the project
```bash
git clone https://gitlab.com/ghostd/jenkins-plasma-applet.git
```
Run the install script
```bash
./install.sh
```
## How can i set the browser used to view the job?

Qt uses the [user's desktop preferences](http://qt-project.org/doc/qt-4.8/qml-qt.html#openUrlExternally-method) to open the URLs. You can change this with the following command line (this example set iceweasel as default browser):

```bash
xdg-settings set default-web-browser iceweasel.desktop
```

## Licences

Copyright 2014 Vincent Ricard

The source files are licenced under the [GPLv3](https://www.gnu.org/licenses/gpl.html)

The SVG artworks come from the official [Jenkins project](http://jenkins-ci.org/) and are licenced under the [MIT](https://github.com/jenkinsci/jenkins/raw/master/LICENSE.txt)
