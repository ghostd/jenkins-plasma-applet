import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

Item {
	id: jobItem
	property string name
	property string svgStatus
	property string url

	height: container.childrenRect.height + padding.margins.top + padding.margins.bottom
	width: parent.width

	PlasmaCore.FrameSvgItem {
		id: padding
		imagePath: "widgets/viewitem"
		prefix: "hover"
		opacity: 0
		anchors.fill: parent
	}

	MouseArea {
		id: container
		anchors {
			fill: parent
			topMargin: padding.margins.top
			leftMargin: padding.margins.left
			rightMargin: padding.margins.right
			bottomMargin: padding.margins.bottom
		}

		hoverEnabled: true
		onEntered: {
			jobsDialog.currentIndex = index;
			jobsDialog.highlightItem.opacity = 1;
		}
		onExited: {
			jobsDialog.highlightItem.opacity = 0;
		}

		onClicked: {
			Qt.openUrlExternally(jobItem.url);
		}

		Image {
			id: jobIcon
			source: svgStatus
			fillMode: Image.PreserveAspectFit
			smooth: true
			width: theme.iconSizes.dialog
			height: width
			anchors {
				left: parent.left
				top: parent.top
			}
		}

		PlasmaComponents.Label {
			id: jobName
			wrapMode: Text.WordWrap
			// TODO:use a vertival alignment
			anchors {
				top: parent.top
				topMargin: 5
				left: jobIcon.right
				leftMargin: 3
			}
			text: jobItem.name
		}
	}
}

// kate: space-indent off; indent-width 4; replace-tabs off; mixed-indent off;
