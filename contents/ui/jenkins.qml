import QtQuick 1.1

import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.extras 0.1 as PlasmaExtras
import org.kde.plasma.graphicswidgets 0.1 as PlasmaWidgets

Item {
	id: jenkinsapplet
	property int minimumWidth: 290
	property int minimumHeight: 340

	// URL used by the UI and the config
	property string jenkinsUrl
	// Actual URL used to retrieve the data
	property string xmlJenkinsUrl
	property int refreshTimerInterval

	PlasmaCore.Theme {
		id: theme
	}

	function popupEventSlot(popped) {
		if (!popped) {
			jobsDialog.currentIndex = -1;
		}
	}

    Component.onCompleted: {
		plasmoid.addEventListener('ConfigChanged', configChanged);
		plasmoid.popupEvent.connect(popupEventSlot);
		plasmoid.aspectRatioMode = IgnoreAspectRatio;
	}

	function configChanged() {
		jenkinsUrl = plasmoid.readConfig('jenkinsUrl');

		if (jenkinsUrl && jenkinsUrl.match(/\w+/)) {
			xmlJenkinsUrl = jenkinsUrl + '/api/xml';

			var intervalInMinutes = plasmoid.readConfig('refreshIntervalInMinutes');
			if (intervalInMinutes == null || intervalInMinutes == '') {
				intervalInMinutes = 1;
			}
			refreshTimerInterval = intervalInMinutes * 60 * 1000;
		}
	}

	function updateTooltip() {
		var tooltip = new Object;
		var globalStatus = computeGlobalStatus();
		var image = plasmoid.file('images', globalStatus + '.svg');
		tooltip['image'] = image;
		if (jobsModel.count == 0) {
			tooltip['mainText'] = i18n('No jobs for %1', jenkinsUrl);
		} else {
			tooltip['mainText'] = i18n('%2 jobs for %1', jenkinsUrl, jobsModel.count);
		}
		plasmoid.popupIconToolTip = tooltip;

		plasmoid.popupIcon = QIcon(image);
	}

	function colorToStatus(color) {
		var status = 'unknown';

		// Remove the potential "_anime" suffix
		color = color.replace((/_anime/), "")

		if (color == 'blue') {
			status = 'success';
		} else if (color == 'yellow') {
			status = 'unstable';
		} else if (color == 'red') {
			status = 'failure';
		} else if (color == 'notbuilt') {
			status = 'notbuilt';
		} else if (color == 'aborted') {
			status = 'aborted';
		} else if (color == 'disabled') {
			status = 'disabled';
		}

		return status;
	}

	function computeGlobalStatus() {
		var status = 'unknown';
		var length = jobsModel.count;

		for (var i = 0; i < length; i++) {
			status = combineStatus(status, colorToStatus(jobsModel.get(i).color));
		}

		return status;
	}

	// We keep the worst status
	function combineStatus(currentGlobalStatus, newStatus) {
		// This array is ordered : the best status is the first
		var allStatus = ['unknown', 'success', 'unstable', 'failure', 'notbuilt', 'aborted', 'disabled'];
		var currentGlobalIndex = allStatus.indexOf(currentGlobalStatus);
		var newIndex = allStatus.indexOf(newStatus);

		// Not optimal, but it works
		return allStatus[Math.max(currentGlobalIndex, newIndex)];
	}

	XmlListModel {
		id: jobsModel
		source: xmlJenkinsUrl
		query: '//job'

		XmlRole { name: 'name'; query: 'name/string()' }
		XmlRole { name: 'url'; query: 'url/string()' }
		XmlRole { name: 'color'; query: 'color/string()' }

		onStatusChanged: {
			if (status == XmlListModel.Ready) {
				updateTooltip();
				refreshJobsTimer.restart();
			}
		}
	}

	Timer {
		id: refreshJobsTimer
		interval: refreshTimerInterval
		onTriggered: jobsModel.reload();
	}

	MouseArea {
		anchors {
			fill: parent
		}

		PlasmaComponents.Label {
			id: header
			text: 0 < jobsModel.count ? i18n('%2 jobs for %1', jenkinsUrl, jobsModel.count) : i18n('No jobs for %1', jenkinsUrl)
			anchors {
				top: parent.top;
				topMargin: 3;
				left: parent.left;
				right: parent.right
			}
			horizontalAlignment: Text.AlignHCenter
		}

		PlasmaCore.Svg {
			id: lineSvg
			imagePath: 'widgets/line'
		}
		PlasmaCore.SvgItem {
			id: headerSeparator
			svg: lineSvg
			elementId: 'horizontal-line'
			anchors {
				top: header.bottom
				topMargin: 3
				left: parent.left
				right: parent.right
			}
			height: lineSvg.elementSize('horizontal-line').height
		}

		PlasmaExtras.ScrollArea {
			anchors {
				top : headerSeparator.bottom
				topMargin: 10
				left: parent.left
				right: parent.right
				bottom: parent.bottom
			}

			ListView {
				id: jobsDialog
				height: parent.height

				model: jobsModel
				delegate: jobItem
				highlight: PlasmaComponents.Highlight {
					hover: true
				}
				Component.onCompleted: currentIndex = -1
			}
		}

		Component {
			id: jobItem
			JobItem {
				name: model['name']
 				svgStatus: 'plasmapackage:/images/' + colorToStatus(model['color']) + '.svg'
				url: model['url']
			}
		}
	} // MouseArea
}

// kate: space-indent off; indent-width 4; replace-tabs off; mixed-indent off;
