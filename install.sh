#!/bin/bash

APPLET_NAME='jenkins'
ZIP_NAME="${APPLET_NAME}.plasmoid"

work=`dirname $0`

cd "$work"
rm -f "../$ZIP_NAME"
zip -r "../$ZIP_NAME" . -x ".git/*"
cd ..

plasmapkg -r "$APPLET_NAME" && kbuildsycoca4
plasmapkg -i "$ZIP_NAME" && kbuildsycoca4
